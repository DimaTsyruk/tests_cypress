# Podorozhnyk pharmacy test

### automated test for a site Podorozhnyk Pharmacy on js using cypress

## Test Cases:

1. **Footer Test —** Verify that following links from footer works.

### Steps:

- Navigate to the home page
- Click on a button to navigate to the page ("About us", "Contacts" and "Team") on website footer
- Verify that the user is redirected to the correct page and that the page loads successfully
- Click on a button to navigate back to the home page
- Verify that the user is redirected back to the home page and that the page loads successfully

2. **Search Test -** Verify that the product can be found using the search bar

### Steps:

- Navigate to the menu page
- Enter the product name in the search bar (e.g."Цитрамон-Дарниця")
- Click on a search button
- Verify that the user is redirected to the correct page and that the page loads successfully
- Verify product name and price
- Click on a button to navigate back to the home page
- Verify that the user is redirected back to the home page and that the page loads successfully

3. **Add to Cart Test —** Verify that a user is able to add a product to the cart.

### Steps:

- Navigate to the menu page
- Enter the product name in the search bar (e.g."Цитрамон-Дарниця")
- Click on a search button
- Click on the "Add to the cart" button
- Click on the "Go to cart menu" button
- Verify that the user is redirected to the cart page and that the page loads successfully
- Click on a button to navigate to the checkout page
- Verify that the user is redirected to the checkout page and that the page loads successfully

## how to run the test:

clone the project to your computer  
install dependencies using the command `npm install`  
run Cypress using the command `cypress open`  
the cypress window will open, where you can run tests and view reports  

## test run example

![Photo](podorozhnyk_test_example.jpeg)

![Video](podorozhnyk_test_example.mp4)
