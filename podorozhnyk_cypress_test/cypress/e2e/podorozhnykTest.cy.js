import HomePage from './PageObjects/HomePage';
import MainPage from './PageObjects/MainPage';
import SearchPage from './PageObjects/SearchPage';
import CartPage from './PageObjects/CartPage';

const PODOROZHNYK_URL = 'https://podorozhnyk.com/';

describe('Podorozhnyk test', () => {
  const homePage = new HomePage();
  const mainPage = new MainPage();
  const searchPage = new SearchPage();
  const cartPage = new CartPage();

  beforeEach(() => {
    mainPage.open();
  });

  it('Podorozhnyk Navigation', () => {
    //About us
    homePage.navigteToAboutUsPage();
    mainPage.verifyCorrectnessOfThePage(`${PODOROZHNYK_URL}company/about/`);
    homePage.returnToHomePage();
    //Contacts
    homePage.navigteToContactsPage();
    mainPage.verifyCorrectnessOfThePage(`${PODOROZHNYK_URL}company/contacts/`);
    homePage.returnToHomePage();
    //Teams
    homePage.navigteToTeamPage();
    mainPage.verifyCorrectnessOfThePage(`${PODOROZHNYK_URL}team/`);
    homePage.returnToHomePage();
  });

  it('Search Test', () => {
    homePage.searchProduct('Цитрамон-Дарниця');
    searchPage.checkSearchTitle('Цитрамон-Дарниця');
    searchPage.checkProductName('Цитрамон-Дарниця знеболюючі таблетки, 6 шт.');
    searchPage.checkProductPrice('14,12');
    homePage.returnToHomePage();
    mainPage.verifyCorrectnessOfThePage(`${PODOROZHNYK_URL}`);
  });

  it('Add to Cart Test', () => {
    homePage.searchProduct('Цитрамон-Дарниця');
    searchPage.checkSearchTitle('Цитрамон-Дарниця');
    searchPage.addToCart();
    cartPage.chooseDeliveryMethod();
    cartPage.addToCart();
    cartPage.goToCart();
    mainPage.verifyCorrectnessOfThePage(`${PODOROZHNYK_URL}basket/`);
    cartPage.checkNumberOfProducts('1');
    cartPage.checkPriceOfProducts('14,12');
    cartPage.confirmTheOrderOnTheSite();
    mainPage.verifyCorrectnessOfThePage(`${PODOROZHNYK_URL}checkout/`);
  });
});
