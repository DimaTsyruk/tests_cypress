class SearchPage {
  searchPageElements = {
    searchTitle: () => cy.get('.search__title'),
    productName: () => cy.get(':nth-child(1) > .product-card__body > .product-card__name > .text-decoration-none'),
    productPrice: () => cy.get(':nth-child(1) > .product-card__body > .product-card__price > .price > .price__value'),
    addToCartButton: () => cy.get(':nth-child(1) > .product-card__body > .to-basket'),
  };

  checkSearchTitle(expectedName) {
    this.searchPageElements.searchTitle().should('contain', expectedName);
  }

  checkProductName(expectedName) {
    this.searchPageElements.productName().should('contain', expectedName);
  }

  checkProductPrice(expectedPrice) {
    this.searchPageElements.productPrice().should('contain', expectedPrice);
  }

  addToCart() {
    this.searchPageElements.addToCartButton().click();
  }
}

export default SearchPage;
