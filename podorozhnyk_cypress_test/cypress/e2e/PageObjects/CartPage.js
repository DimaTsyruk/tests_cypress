class CartPage {
  cartPageElements = {
    novaPostButton: () => cy.get(':nth-child(2) > .item__body'),
    addToCartButton: () => cy.get('.delivery-method__to-confirm'),
    goToCartButton: () => cy.get('.basket__controls > .btn-default'),
    numberOfProducts: () => cy.get('.form-control'),
    ProductPrice: () => cy.get('.basket-position__prices > .price > .price__value'),
    checkoutButton: () => cy.get('.checkout__footer > .btn-default'),
  };

  chooseDeliveryMethod() {
    this.cartPageElements.novaPostButton().click();
  }

  addToCart() {
    this.cartPageElements.addToCartButton().click();
  }

  goToCart() {
    this.cartPageElements.goToCartButton().click();
  }

  checkNumberOfProducts(expectedNumber) {
    this.cartPageElements.numberOfProducts().should('have.value', expectedNumber);
  }

  checkPriceOfProducts(expectedPrice) {
    this.cartPageElements.ProductPrice().should('contain', expectedPrice);
  }

  confirmTheOrderOnTheSite() {
    this.cartPageElements.checkoutButton().click();
  }
}

export default CartPage;
