class HomePage {
  homePageElements = {
    aboutUsButton: () => cy.get(':nth-child(2) > .navigation-group__items > :nth-child(1) > a'),
    contactsButton: () => cy.get(':nth-child(2) > .navigation-group__items > :nth-child(2) > a'),
    teamButton: () => cy.get(':nth-child(2) > .navigation-group__items > :nth-child(3) > a'),
    logoButton: () => cy.get('.col > .justify-content-between > .main-logo > img'),
    searchBar: () => cy.get('.search__input'),
    searchButton: () => cy.get('.search__to-search-results'),
  };

  navigteToAboutUsPage() {
    this.homePageElements.aboutUsButton().click();
  }

  navigteToContactsPage() {
    this.homePageElements.contactsButton().click();
  }

  navigteToTeamPage() {
    this.homePageElements.teamButton().click();
  }

  returnToHomePage() {
    this.homePageElements.logoButton().click();
  }

  searchProduct(productName) {
    this.homePageElements.searchBar().type(productName);
    this.homePageElements.searchButton().click();
  }
}

export default HomePage;
