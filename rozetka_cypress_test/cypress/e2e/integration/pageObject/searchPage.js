class SearchPage {
  searchPageElements = {
    firstItem: () => cy.get("img.ng-lazyloaded").first(),
    addToCartButton: () => cy.xpath('//button[@aria-label="Купити"]').first(),
    cartButton: () => cy.xpath('//button[@class="header__button ng-star-inserted"]').eq(1),
  };

  verifyCorrectnessOfTheSearch(keyword) {
    cy.get("body").should("be.visible");
    cy.url().should("include", keyword);
  }
  goToFirstItem() {
    this.searchPageElements.firstItem().click();
  }
  addToCart() {
    this.searchPageElements.addToCartButton().click();
  }
  goToCartPage() {
    this.searchPageElements.cartButton().click();
  }
}

export default SearchPage;
