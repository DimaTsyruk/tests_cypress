class CheckoutPage {
  loginFormElements = {
    usernameInput: () => cy.xpath('//input[@id="recipientName"]'),
    lastnameInput: () => cy.xpath('//input[@id="recipientSurname"]'),
    phoneNumberInput: () => cy.xpath('//input[@id="checkoutUserPhone"]'),
    chooseBuyerButton: () => cy.xpath('//button[@class = "select-css select-css--medium"]'),
    chooseFirstBuyer: () => cy.xpath(' //div[@class = "autocomplete__item"]').first(),
    placeOrderButton: () => cy.xpath('//input[@value="Замовлення підтверджую"]'),
  };

  enterName(name) {
    this.loginFormElements.usernameInput().type(name);
  }
  enterLastName(lastname) {
    this.loginFormElements.lastnameInput().type(lastname);
  }
  enterPhoneNumber(phoneNumber) {
    this.loginFormElements.phoneNumberInput().type(phoneNumber);
  }
  chooseBuyer() {
    this.loginFormElements.chooseBuyerButton().click();
    this.loginFormElements.chooseFirstBuyer().click();
  }
  placingOrder() {
    this.loginFormElements.placeOrderButton().click();
  }
  verifyConfirmationPage(url) {
    cy.url().should("eq", url);
  }
}

export default CheckoutPage;
