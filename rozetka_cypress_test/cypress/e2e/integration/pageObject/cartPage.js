class CartPage {
  cartElements = {
    quantityOfGoods: () => cy.xpath('//input[@data-testid="cart-counter-input"]'),
    priceOfGoods: () => cy.xpath('//div/div[1]/div/div/div/span'),
    nameOfGoods: () => cy.xpath(' //a[@class="cart-product__title"]'),
    checkoutButton: () => cy.xpath('//a[@data-testid="cart-receipt-submit-order"]'),
    increaseQuantityButton: () => cy.xpath('//button[@data-testid="cart-counter-increment-button"]'),
    menuCartButton: () => cy.xpath(' //button[@id="cartProductActions0"]'),
    deleteButton: () => cy.xpath('//button[@class="button button--medium button--with-icon button--link"]'),
    cartMessage: () => cy.xpath('//h4[@class="cart-dummy__heading"]'),
  };

  verifyQuantity(quantity) {
    this.cartElements.quantityOfGoods().should('have.value', quantity);
  }
  verifyPrice(price) {
    this.cartElements.priceOfGoods().invoke('parent').invoke('text').should('contain', price);
  }
  verifyName(name) {
    this.cartElements.nameOfGoods().should('be.visible').and('contain', name);
  }
  checkout() {
    this.cartElements.checkoutButton().click();
  }
  increaseTheQuantity() {
    this.cartElements.increaseQuantityButton().click();
  }
  removeProduct() {
    this.cartElements.menuCartButton().click();
    this.cartElements.deleteButton().click();
  }
  verifyRemoving() {
    this.cartElements.cartMessage().should('be.visible').and('contain', 'Кошик порожній');
  }
}

export default CartPage;
