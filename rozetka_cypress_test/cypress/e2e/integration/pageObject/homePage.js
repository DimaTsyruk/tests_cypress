class HomePage {
  homePageElements = {
    menuButton: () => cy.get('svg.ng-tns-c33-1'),
    aboutUsButton: () => cy.get('a[class="ng-tns-c31-2"]').first(),
    logoBtn: () => cy.get('img[alt="Rozetka Logo"]'),
    searchBar: () => cy.get('.search-form__input'),
    searchButton: () => cy.get('.search-form > .button'),
    searchField: () => cy.xpath('//input[@name="search"]'),
    search: () => cy.xpath('//button[@class="button button_color_green button_size_medium search-form__submit ng-star-inserted"]'),
    authorizationButton: () => cy.xpath('//button[@class="header__button ng-star-inserted"]').first(),
  };

  open() {
    cy.visit('https://rozetka.com.ua/');
  }
  navigteToAboutUsPage() {
    this.homePageElements.menuButton().click();
    this.homePageElements.aboutUsButton().click();
  }
  verifyCorrectnessOfThePage(expectedUrl) {
    cy.get('body').should('be.visible');
    cy.url().should('eq', expectedUrl);
  }
  navigateToHomePage() {
    this.homePageElements.logoBtn().click();
  }
  searchProduct(productName) {
    this.homePageElements.searchBar().type(productName);
    this.homePageElements.searchButton().click();
  }
  productSearch(productName) {
    this.homePageElements.searchField().type(productName);
    this.homePageElements.search().click();
  }
  navigateToAuthorizationPage() {
    this.homePageElements.authorizationButton().click();
  }
  waitUntilResultsLoaded() {
    cy.wait(2000);
  }
  scrollToResults() {
    cy.scrollTo(0, 200);
  }
}

export default HomePage;
