class AuthorizationPage {
  userAuthElements = {
    emailInput: () => cy.xpath('//input[@id="auth_email"]'),
    passwordInput: () => cy.xpath('//input[@id="auth_pass"]'),
    loginBtn: () => cy.xpath('//button[@class="button button--large button--green auth-modal__submit ng-star-inserted"]'),
    errorMessage: () => cy.xpath('//p[@class="error-message ng-star-inserted"]'),
    captcha: () => cy.xpath('//re-captcha[@id="ngrecaptcha-0"]'),
  };

  enterEmail(email) {
    this.userAuthElements.emailInput().clear().type(email);
  }
  enterPassword(password) {
    this.userAuthElements.passwordInput().clear().type(password);
  }
  login() {
    this.userAuthElements.loginBtn().click();
  }
  verifyErrorMessage() {
    this.userAuthElements.errorMessage().should("be.visible").and("contain", "Введено невірну адресу ел. пошти або номер телефону");
  }
  verifySuccessfullySubmittedForm() {
    this.userAuthElements.captcha().should("be.visible");
  }
}

export default AuthorizationPage;
