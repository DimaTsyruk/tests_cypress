import HomePage from './pageObject/homePage';
import SearchPage from './pageObject/searchPage';
import CartPage from './pageObject/cartPage';
import CheckoutPage from './pageObject/checkoutPage';
import AuthorizationPage from './pageObject/authorizationPage';

describe('Rozetka test', () => {
  const homePage = new HomePage();
  const searchPage = new SearchPage();
  const cartPage = new CartPage();
  const checkoutPage = new CheckoutPage();
  const authorizationPage = new AuthorizationPage();

  beforeEach(() => {
    homePage.open();
    homePage.waitUntilResultsLoaded();
  });

  it('Rozetka Navigation', () => {
    homePage.navigteToAboutUsPage();
    homePage.verifyCorrectnessOfThePage('https://rozetka.com.ua/ua/pages/about/');
    homePage.navigateToHomePage();
    homePage.verifyCorrectnessOfThePage('https://rozetka.com.ua/ua/');
  });

  it('Rozetka Search', () => {
    homePage.searchProduct('iphone 13');
    searchPage.verifyCorrectnessOfTheSearch('iphone+13');
    searchPage.goToFirstItem();
    searchPage.verifyCorrectnessOfTheSearch('apple_iphone_13');
  });

  it('Adding to cart', () => {
    homePage.productSearch('iphone 13');
    homePage.waitUntilResultsLoaded();
    homePage.scrollToResults();
    searchPage.addToCart();
    searchPage.goToCartPage();
    cartPage.verifyQuantity(1);
    cartPage.verifyPrice('31 499₴');
    cartPage.verifyName('Мобільний телефон Apple iPhone 13 128GB Midnight');
    cartPage.checkout();
    checkoutPage.enterLastName('Шевченко');
    checkoutPage.enterName('Тарас');
    checkoutPage.enterPhoneNumber('0501234567');
    checkoutPage.chooseBuyer();
    checkoutPage.placingOrder();
    checkoutPage.verifyConfirmationPage('https://rozetka.com.ua/ua/checkout/');
  });

  it('Removing from cart test', () => {
    homePage.productSearch('iphone 13');
    homePage.waitUntilResultsLoaded();
    homePage.scrollToResults();
    searchPage.addToCart();
    searchPage.goToCartPage();
    cartPage.verifyQuantity(1);
    cartPage.verifyPrice('31 499₴');
    cartPage.verifyName('Мобільний телефон Apple iPhone 13 128GB Midnight');
    cartPage.increaseTheQuantity();
    cartPage.verifyQuantity(2);
    cartPage.verifyPrice('62 998₴');
    cartPage.removeProduct();
    cartPage.verifyRemoving();
  });

  it('Validation registration page', () => {
    homePage.navigateToAuthorizationPage();
    authorizationPage.enterEmail('t_shevchenko@@gmail.com');
    authorizationPage.enterPassword('password');
    authorizationPage.login();
    authorizationPage.verifyErrorMessage();
    authorizationPage.enterEmail('t_shevchenko@gmail.com');
    authorizationPage.enterPassword('Password123');
    authorizationPage.login();
    authorizationPage.verifySuccessfullySubmittedForm();
  });
});
