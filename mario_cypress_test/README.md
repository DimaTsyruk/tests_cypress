# Mario test

### automated test for a site Mario on js using cypress

## Test Cases:

1. **Navigation Test —** Verify that a user is able to navigate between pages on the website.

### Steps:

Navigate to the home page  
Click on a button to navigate to the page ("About us", "News", "Halls", "Karaoke" and "Contacts") on website header  
Verify that the user is redirected to the correct page and that the page loads successfully  
Click on a button to navigate back to the home page  
Verify that the user is redirected back to the home page and that the page loads successfully  

2. **Laguage Change Test -** Verify that a user is able to change site language.  

### Steps:

Navigate to the home page  
Verify that the language of the site is Ukrainian  
Click on the "EN" button to change the language  
Verify that the language of the site is English  

3. **Restaurant Menu Test —** Verify that the menu page is displayed correctly  

### Steps:

Navigate to the home page  
Click on a button to navigate to the "Menu" page  
Verify that the user is redirected to the correct page and that the page loads successfully  
Click on a dish to learn more about it  
Verify that the information about the dish is displayed correctly (e.g. name, price, quantity, etc.)  
Click on the button to return to the main menu  
Verify that the user is redirected to the correct page and that the page loads successfully  

4. **Add to Favorites Test -** Verify that a user is able to add a product to "Favorite".  

### Steps:

Navigate to the menu page  
Click on the "Add to Favorites" button on some item  
Navigate to the favorites page  
Verify that the added item is displayed in the cart  

5. **Add to Delivery Cart Test -** Verify that a user is able to add a product to the delivery cart and change the quantity of the item.  

### Steps:

Navigate to the menu page  
Click on the "Add" button on some item  
Increase the quantity of the item in the cart by clicking the "+" button  
Navigate to the delivery cart page  
Click on the "Confirm" button  
Verify that the added items are displayed in the cart  
Click on the "Continue" button  
Сhoose delivery method "Takeaway"  
Verify that the shipping method has changed  
Click on a button to navigate back to the menu page  

6. **Remove from Cart Test —** Verify that a user is able to remove a item from their shopping cart by clicking on the "Remove" button.  

### Steps:

Navigate to the menu page  
Navigate to the delivery cart page  
Decrease the quantity of the item in the cart by clicking the "-" button  
Verify that the quantity and total price have updated  
Remove product from the cart  
Verify that product has been removed  

## how to run the test:

- clone the project to your computer  
- install dependencies using the command `npm install`  
- run Cypress using the command `cypress open`  
- the cypress window will open, where you can run tests and view reports  

## test run example  

![Photo](mario_test_example.jpeg) 

![Video](mario_test_example.mp4)
