class MenuPage {
  menuPageElements = {
    syrnykyAdditionalInformation: () => cy.get('.styles_ProductTitle__WjQ9T').contains('Сирники з вареним згущеним молоком і сметаною'),
    nameOfSyrnyky: () => cy.get('div[class="styles_title__9CDSI"]'),
    priceOfSyrnyky: () => cy.get('div[class="styles_price__kcjtW"]'),
    returnToMenuPageButton: () => cy.get('button[class="styles_button__eH6h8 styles_appearancePrimary__NVNw2 styles_closeButton__fz06o"]'),
    addToFavoritesButton: () => cy.xpath('//*[@id="device-type-selector"]/div[2]/div/div/div/div[3]/div[5]/div/div').first(),
    favoriteItemsButton: () => cy.xpath('//*[@id="device-type-selector"]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div/div/a/button'),
    itemFromFavorites: () => cy.get('div[class="styles_ProductTitle__WjQ9T"]'),
    quantityItemsOnCart: () => cy.get('div[class="styles_number__qH7TK"]'),
    totalPrice: () => cy.get('div[class="styles_PriceDiscount__Kccnr"]'),
    shippingMethodButton: () => cy.get('div[class="styles_option___ZCkd"]'),
    quantityItems: () => cy.get('div[class="styles_number__qH7TK"]'),
    openCartButton: () =>
      cy.get(
        'button[class="styles_button__eH6h8 styles_appearancePrimary__NVNw2 styles_withShadow___6J2y styles_desktop-cart-button__9MvXM"]'
      ),
    increaseQuantityButton: () =>
      cy.get('button[class="styles_button__eH6h8 styles_sizeSmall__PbM78 styles_appearanceStroke__2w0V8 styles_withShadow___6J2y"]'),
    addToCartButton: () =>
      cy
        .get(
          'button[class="styles_button__eH6h8 styles_sizeSmall__PbM78 styles_appearancePrimary__NVNw2 styles_fullWidth__n1V8K styles_ProductBottomBtnAdd__yDt5S"]'
        )
        .first(),
    confirmCartButton: () =>
      cy.get(
        'button[class="styles_button__eH6h8 styles_sizeLarge__0cvOx styles_appearancePrimary__NVNw2 styles_cart-desktop-modal-body-bottom-button__rIW9i"]'
      ),
    contineCartButton: () =>
      cy.get('button[class="styles_button__eH6h8 styles_sizeLarge__0cvOx styles_appearancePrimary__NVNw2 styles_fullWidth__n1V8K"]'),
    delateFromCartButton: () =>
      cy.get(
        'button[class="styles_button__eH6h8 styles_sizeSmall__PbM78 styles_appearanceStroke__2w0V8 styles_withShadow___6J2y styles_danger__QWUB7"]'
      ),
    cartMessage: () =>
      cy.get(
        'button[class="styles_button__eH6h8 styles_appearancePrimary__NVNw2 styles_withShadow___6J2y styles_disabled___u82U styles_desktop-cart-button__9MvXM styles_is-empty__8Hyfh"]'
      ),
  };

  open() {
    cy.visit('https://mario.choiceqr.com/uk/online-menu/618292a911c47552d1c8759c');
  }

  openSyrnykyPage() {
    this.menuPageElements.syrnykyAdditionalInformation().click();
  }

  verifyNameOfItem(expectedName) {
    this.menuPageElements.nameOfSyrnyky().should('have.text', expectedName);
  }

  verifyPriceOfItem(expectedPrice) {
    this.menuPageElements.priceOfSyrnyky().should('have.text', expectedPrice);
  }

  returrnToMenuPage() {
    this.menuPageElements.returnToMenuPageButton().click();
    cy.wait(1000);
  }

  addToFavorite() {
    this.menuPageElements.addToFavoritesButton().click();
  }

  openFavorites() {
    this.menuPageElements.favoriteItemsButton().click();
  }

  verifyNameOfFavoriteItem(expectedName) {
    this.menuPageElements.itemFromFavorites().should('have.text', expectedName);
  }

  addToCart() {
    this.menuPageElements.addToCartButton().click();
  }

  increaseQuantit() {
    this.menuPageElements.increaseQuantityButton().click();
  }

  openCart() {
    this.menuPageElements.openCartButton().click();
  }

  confirmCart() {
    this.menuPageElements.confirmCartButton().click();
  }

  verifyQuantityOnCart(expectedQuantity) {
    this.menuPageElements.quantityItemsOnCart().should('have.text', expectedQuantity);
  }

  verifyTotalPriceOnCart(expectedPrice) {
    this.menuPageElements.totalPrice().should('have.text', expectedPrice);
  }

  contineCart() {
    this.menuPageElements.contineCartButton().click();
  }

  choiceShippingMethod() {
    this.menuPageElements.shippingMethodButton().first().click();
  }

  choiceTakeaway() {
    this.menuPageElements.shippingMethodButton().eq(1).click();
  }

  verifyShippingMethod(expectedMethod) {
    this.menuPageElements.shippingMethodButton().first().should('have.text', expectedMethod);
  }

  verifyQuantityOnMenuPage(expectedQuantity) {
    this.menuPageElements.quantityItems().should('have.text', expectedQuantity);
  }

  decreaseQuantit() {
    this.menuPageElements.increaseQuantityButton().first().click();
  }

  delateFromCart() {
    this.menuPageElements.delateFromCartButton().click();
  }

  isEmptyCart(expectedText) {
    this.menuPageElements.cartMessage().should('have.text', expectedText);
  }
}

export default MenuPage;
