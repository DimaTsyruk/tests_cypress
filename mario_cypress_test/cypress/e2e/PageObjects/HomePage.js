class HomePage {
  homePageElements = {
    aboutUsButton: () => cy.get('a[class="menu__link_2K2 w-nav__link"]').contains('Про нас'),
    menuButton: () => cy.get('a[class="menu__link_2K2 w-nav__link"]').contains('Меню'),
    newsButton: () => cy.get('a[class="menu__link_2K2 w-nav__link"]').contains('Новини'),
    hallsButton: () => cy.get('a[class="menu__link_2K2 w-nav__link"]').contains('Зали'),
    karaokeButton: () => cy.get('a[class="menu__link_2K2 w-nav__link"]').contains('Караоке'),
    contactsButton: () => cy.get('a[class="menu__link_2K2 w-nav__link"]').contains('Контакти'),
    logoButton: () => cy.get('img[data-component="logo"]').first(),
    enLenguageButton: () => cy.get('a[data-slug-id="62e5189d69775d000d0b150e"]').first(),
    marioHeading: () => cy.xpath('//section/div[3]//h2/span'),
  };

  open() {
    cy.visit('https://mario-restaurant.kiev.ua/');
  }

  returnToHomePage() {
    this.homePageElements.logoButton().click();
  }

  navigteToAboutUsPage() {
    this.homePageElements.aboutUsButton().click();
  }

  navigteToNewsPage() {
    this.homePageElements.newsButton().click();
  }

  navigteToHallsPage() {
    this.homePageElements.hallsButton().click();
  }

  navigteToKaraokePage() {
    this.homePageElements.karaokeButton().click();
  }

  navigteToContactsPage() {
    this.homePageElements.contactsButton().click();
  }

  verifyCorrectnessOfThePage(expectedUrl) {
    cy.get('body').should('be.visible');
    cy.url().should('eq', expectedUrl);
  }

  verifyHeadingLanguage(expectedText) {
    this.homePageElements.marioHeading().should('have.text', expectedText);
  }

  chooseEnLenguage() {
    this.homePageElements.enLenguageButton().click();
  }

  navigteToMenuPage() {
    this.homePageElements.menuButton().click();
  }
}

export default HomePage;
