class MainPage {
  verifyCorrectnessOfThePage(expectedUrl) {
    cy.get('body').should('be.visible');
    cy.url().should('eq', expectedUrl);
  }
}

export default MainPage;
