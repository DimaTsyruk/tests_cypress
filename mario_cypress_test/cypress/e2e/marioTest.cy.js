import HomePage from './PageObjects/HomePage';
import MenuPage from './PageObjects/MenuPage';
import MainPage from './PageObjects/MainPage';

const MARIO_URL = 'https://mario-restaurant.kiev.ua/';
const MARIO_MENU_URL = 'https://mario.choiceqr.com/uk/online-menu/section:menyu-restoranu-305/';

describe('Mario test', () => {
  const homePage = new HomePage();
  const menuPage = new MenuPage();
  const mainPage = new MainPage();

  it('Mario Navigation', () => {
    homePage.open();
    // About Us Page
    homePage.navigteToAboutUsPage();
    mainPage.verifyCorrectnessOfThePage(`${MARIO_URL}pro-nas`);
    homePage.returnToHomePage();
    mainPage.verifyCorrectnessOfThePage(`${MARIO_URL}`);
    // News Page
    homePage.navigteToNewsPage();
    mainPage.verifyCorrectnessOfThePage(`${MARIO_URL}novini`);
    homePage.returnToHomePage();
    mainPage.verifyCorrectnessOfThePage(`${MARIO_URL}`);
    // Halls Page
    homePage.navigteToHallsPage();
    mainPage.verifyCorrectnessOfThePage(`${MARIO_URL}zali`);
    homePage.returnToHomePage();
    mainPage.verifyCorrectnessOfThePage(`${MARIO_URL}`);
    // Karaoke Page
    homePage.navigteToKaraokePage();
    mainPage.verifyCorrectnessOfThePage(`${MARIO_URL}karaoke`);
    homePage.returnToHomePage();
    mainPage.verifyCorrectnessOfThePage(`${MARIO_URL}`);
    // Contacts Page
    homePage.navigteToContactsPage();
    mainPage.verifyCorrectnessOfThePage(`${MARIO_URL}kontakti`);
    homePage.returnToHomePage();
    mainPage.verifyCorrectnessOfThePage(`${MARIO_URL}`);
  });

  it('Mario Laguage Changing', () => {
    homePage.open();
    homePage.verifyHeadingLanguage('ЛЕГЕНДАРНИЙІТАЛІЙСЬКИЙ РЕСТОРАН');
    homePage.chooseEnLenguage();
    homePage.verifyHeadingLanguage('Legendary Italian restaurant');
  });

  it('Mario Menu Test', () => {
    menuPage.open();
    menuPage.openSyrnykyPage();
    menuPage.verifyNameOfItem('Сирники з вареним згущеним молоком і сметаною');
    menuPage.verifyPriceOfItem('185 UAH');
    menuPage.returrnToMenuPage();
    mainPage.verifyCorrectnessOfThePage(`${MARIO_MENU_URL}snidanki`);
  });

  it('Mario Adding to Favorites', () => {
    menuPage.open();
    menuPage.openSyrnykyPage();
    menuPage.addToFavorite();
    menuPage.returrnToMenuPage();
    menuPage.openFavorites();
    menuPage.verifyNameOfFavoriteItem('Сирники з вареним згущеним молоком і сметаною');
  });

  it('Mario Adding to Delivery Cart', () => {
    menuPage.open();
    menuPage.addToCart();
    menuPage.increaseQuantit();
    menuPage.openCart();
    menuPage.confirmCart();
    menuPage.verifyQuantityOnCart('2');
    menuPage.verifyTotalPriceOnCart('370 UAH');
    menuPage.contineCart();
    menuPage.choiceShippingMethod();
    menuPage.choiceTakeaway();
  });

  it('Mario Removing from Cart', () => {
    menuPage.open();
    menuPage.addToCart();
    menuPage.increaseQuantit();
    menuPage.verifyQuantityOnMenuPage('2');
    menuPage.decreaseQuantit();
    menuPage.verifyQuantityOnMenuPage('1');
    menuPage.delateFromCart();
    menuPage.isEmptyCart('Корзина порожня');
  });
});
